package com.sezer.moneytransfer.exception;

public class MoneyTransferException extends Exception {
    private String message;

    public MoneyTransferException(String message){
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
