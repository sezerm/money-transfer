package com.sezer.moneytransfer.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sezer.moneytransfer.enums.StatusResponseEnum;
import com.sezer.moneytransfer.exception.MoneyTransferException;
import com.sezer.moneytransfer.service.TransferDetailService;
import com.sezer.moneytransfer.utils.InstanceFactory;
import com.sezer.moneytransfer.vm.request.MoneyTransferVM;
import com.sezer.moneytransfer.vm.response.StandardResponse;
import spark.Route;

public class TransferDetailApi {

    private static TransferDetailService transferDetailService = InstanceFactory.getInstance(TransferDetailService.class);

    public static Route listTransferDetails = (req, res) -> {
        res.type("application/json");
        return new Gson().toJson(transferDetailService.list());
    };

    public static Route transferMoney = (req, res) -> {
        res.type("application/json");
        String body = "" + req.body();
        Gson gson = new GsonBuilder().create();
        MoneyTransferVM moneyTransferVM = gson.fromJson(body, MoneyTransferVM.class);
        try{
            transferDetailService.transferMoney(moneyTransferVM);
            return new Gson().toJson(new StandardResponse(StatusResponseEnum.SUCCESS, "Transfer achieved successfully"));
        }catch (MoneyTransferException e){
            return new Gson().toJson(new StandardResponse(StatusResponseEnum.ERROR, e.getMessage()));
        }
    };
}
