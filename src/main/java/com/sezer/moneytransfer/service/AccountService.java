package com.sezer.moneytransfer.service;

import com.sezer.moneytransfer.dao.AccountDao;
import com.sezer.moneytransfer.entity.Account;
import com.sezer.moneytransfer.utils.InstanceFactory;

import java.util.List;

public class AccountService {

    private static AccountDao accountDao = InstanceFactory.getInstance(AccountDao.class);

    public Account getByAccountNumber(String accountNumber) {
        return accountDao.getByAccountNumber(accountNumber);
    }

    public List<Account> list(){
        return accountDao.list();
    }

    public Account update(Account account) {
        return accountDao.update(account);
    }
}
