package com.sezer.moneytransfer.vm.response;

import com.google.gson.JsonElement;
import com.sezer.moneytransfer.enums.StatusResponseEnum;

public class StandardResponse {

    private String status;
    private String message;
    private JsonElement data;

    public StandardResponse(StatusResponseEnum statusResponseEnum, String message) {
        this.status = statusResponseEnum.getStatus();
        this.message = message;
    }

    public StandardResponse(StatusResponseEnum statusResponseEnum, String message, JsonElement data) {
        this.status = statusResponseEnum.getStatus();
        this.message = message;
        this.data = data;
    }
}
