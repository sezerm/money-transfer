package com.sezer.moneytransfer.service;

import com.sezer.moneytransfer.dao.CurrencyDao;
import com.sezer.moneytransfer.entity.Currency;
import com.sezer.moneytransfer.utils.InstanceFactory;

import java.util.List;

public class CurrencyService {

    private static CurrencyDao currencyDao = InstanceFactory.getInstance(CurrencyDao.class);

    public List<Currency> listByTypeId(Long typeId) {
        return currencyDao.listByTypeId(typeId);
    }

    public List<Currency> list(){
        return currencyDao.list();
    }
}
