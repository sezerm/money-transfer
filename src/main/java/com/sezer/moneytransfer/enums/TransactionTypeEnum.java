package com.sezer.moneytransfer.enums;

public enum TransactionTypeEnum {
    MONEY_TRANSFER(1, "Money Transfer"),
    EXCHANGE(2, "Exchange");

    public static TransactionTypeEnum getById(Integer id) {
        for(TransactionTypeEnum e : values()) {
            if(e.id.equals(id)) return e;
        }
        return null;
    }

    private Integer id;

    private String name;

    TransactionTypeEnum(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
