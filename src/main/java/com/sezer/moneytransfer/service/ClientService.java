package com.sezer.moneytransfer.service;

import com.sezer.moneytransfer.dao.ClientDao;
import com.sezer.moneytransfer.entity.Client;
import com.sezer.moneytransfer.utils.InstanceFactory;

import java.util.List;

public class ClientService {

    private static ClientDao clientDao = InstanceFactory.getInstance(ClientDao.class);

    public List<Client> list(){
        return clientDao.list();
    }
}
