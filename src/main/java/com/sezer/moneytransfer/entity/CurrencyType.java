package com.sezer.moneytransfer.entity;

import lombok.Data;

@Data
public class CurrencyType {
    private Long id;
    private String name;
}
