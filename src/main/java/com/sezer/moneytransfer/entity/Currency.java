package com.sezer.moneytransfer.entity;

import lombok.Data;

@Data
public class Currency {
    private Long id;
    private String name;
    private Integer currencyTypeId;
}
