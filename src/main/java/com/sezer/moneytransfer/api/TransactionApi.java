package com.sezer.moneytransfer.api;

import com.google.gson.Gson;
import com.sezer.moneytransfer.service.TransactionService;
import com.sezer.moneytransfer.service.TransactionTypeService;
import com.sezer.moneytransfer.utils.InstanceFactory;
import spark.Route;

public class TransactionApi {

    private static TransactionService transactionService = InstanceFactory.getInstance(TransactionService.class);

    public static Route listTransactions = (req, res) -> {
        res.type("application/json");
        return new Gson().toJson(transactionService.list());
    };

}
