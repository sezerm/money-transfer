package com.sezer.moneytransfer.service;

import com.sezer.moneytransfer.dao.TransactionDao;
import com.sezer.moneytransfer.entity.Transaction;
import com.sezer.moneytransfer.utils.InstanceFactory;

import java.util.List;

public class TransactionService {

    private static TransactionDao transactionDao = InstanceFactory.getInstance(TransactionDao.class);

    public Long create(Transaction transaction) {
        return transactionDao.create(transaction);
    }

    public List<Transaction> list() {
        return transactionDao.list();
    }
}
