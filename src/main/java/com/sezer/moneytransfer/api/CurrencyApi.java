package com.sezer.moneytransfer.api;

import com.google.gson.Gson;
import com.sezer.moneytransfer.service.CurrencyService;
import com.sezer.moneytransfer.utils.InstanceFactory;
import spark.Route;

public class CurrencyApi {

    private static CurrencyService currencyService = InstanceFactory.getInstance(CurrencyService.class);

    public static Route listCurrenciesByType = (req, res) -> {
        res.type("application/json");
        return new Gson().toJson(currencyService.listByTypeId(Long.valueOf(req.params("type-id"))));
    };

    public static Route listCurrencies = (req, res) -> {
        res.type("application/json");
        return new Gson().toJson(currencyService.list());
    };
}
