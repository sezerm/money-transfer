package com.sezer.moneytransfer.dao;

import com.sezer.moneytransfer.entity.Account;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import java.util.List;

public class AccountDao {

    private Sql2o sql2o;

    public AccountDao(Sql2o sql2o) {
        this.sql2o = sql2o;
    }

    public List<Account> list() {
        try (Connection conn = sql2o.beginTransaction()) {
            List<Account> accounts = conn.createQuery("select * from Account")
                    .executeAndFetch(Account.class);
            return accounts;
        }
    }

    public Account get(Long id) {
        try (Connection conn = sql2o.beginTransaction()) {
            Account account = conn.createQuery("select * from Account a where a.id=:id")
                    .addParameter("id", id)
                    .executeAndFetchFirst(Account.class);
            return account;
        }
    }

    public Account getByAccountNumber(String accountNumber) {
        try (Connection conn = sql2o.beginTransaction()) {
            Account account = conn.createQuery("select * from Account a where a.accountNumber=:accountNumber")
                    .addParameter("accountNumber", accountNumber)
                    .executeAndFetchFirst(Account.class);
            return account;
        }
    }

    public boolean delete(Long id) {
        try (Connection conn = sql2o.beginTransaction()) {
            int result = conn.createQuery("delete from Account a where a.id=:id")
                    .addParameter("id", id)
                    .executeUpdate().getResult();
            return result>0;
        }
    }

    public Account create(Account account) {
        String sql = "insert into Account(clientId, currencyId, asset, name, accountNumber) values (:clientId, :currencyId, :asset, :name, :accountNumber)";
        try (Connection conn = sql2o.open()) {
            Object key = conn.createQuery(sql).bind(account).executeUpdate().getKey();
            return get(Long.parseLong(String.valueOf(key)));
        }
    }

    public Account update(Account account) {
        String sql = "update Account set clientId = :clientId, currencyId = :currencyId, asset = :asset, name = :name, accountNumber = :accountNumber where id = :id";
        try (Connection conn = sql2o.open()) {
            conn.createQuery(sql).bind(account).executeUpdate();
            return get(account.getId());
        }
    }
}
