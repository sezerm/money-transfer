package com.sezer.moneytransfer.dao;

import com.sezer.moneytransfer.entity.Transaction;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

public class TransactionDao {

    private Sql2o sql2o;

    public TransactionDao(Sql2o sql2o) {
        this.sql2o = sql2o;
    }

    public List<Transaction> list() {
        try (Connection conn = sql2o.beginTransaction()) {
            List<Transaction> transactions = conn.createQuery("select * from Transaction")
                    .executeAndFetch(Transaction.class);
            return transactions;
        }
    }

    public Transaction get(Long id) {
        try (Connection conn = sql2o.beginTransaction()) {
            Transaction transaction = conn.createQuery("select * from Transaction t where t.id=:id")
                    .addParameter("id", id)
                    .executeAndFetchFirst(Transaction.class);
            return transaction;
        }
    }

    public boolean delete(Long id) {
        try (Connection conn = sql2o.beginTransaction()) {
            int result = conn.createQuery("delete from Transaction t where u.id=:id")
                    .addParameter("id", id)
                    .executeUpdate().getResult();
            return result > 0;
        }
    }

    public Long create(Transaction transaction) {
        transaction.setTransactionTime(Timestamp.from(Instant.now()));
        String sql = "insert into Transaction(transactionTypeId, detail, transactionTime) values (:transactionTypeId, :detail, :transactionTime)";
        try (Connection conn = sql2o.open()) {
            Object key = conn.createQuery(sql).bind(transaction).executeUpdate().getKey();
            return Long.parseLong(String.valueOf(key));
        }
    }
}
