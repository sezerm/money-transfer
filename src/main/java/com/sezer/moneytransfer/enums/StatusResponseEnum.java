package com.sezer.moneytransfer.enums;

public enum StatusResponseEnum {
    SUCCESS("Success"),
    ERROR("Error");

    private String status;

    StatusResponseEnum(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
