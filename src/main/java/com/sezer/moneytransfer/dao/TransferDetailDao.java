package com.sezer.moneytransfer.dao;

import com.sezer.moneytransfer.entity.TransferDetail;
import com.sezer.moneytransfer.utils.Sql2oUtils;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import java.util.List;

public class TransferDetailDao {

    private Sql2o sql2o;

    public TransferDetailDao(Sql2o sql2o) {
        this.sql2o = sql2o;
    }

    public List<TransferDetail> list() {
        try (Connection conn = sql2o.beginTransaction()) {
            List<TransferDetail> transferDetails = conn.createQuery("select * from TransferDetail")
                    .executeAndFetch(TransferDetail.class);
            return transferDetails;
        }
    }

    public TransferDetail get(Long id) {
        try (Connection conn = sql2o.beginTransaction()) {
            TransferDetail transferDetail = conn.createQuery("select * from TransferDetail td where td.id=:id")
                    .addParameter("id", id)
                    .executeAndFetchFirst(TransferDetail.class);
            return transferDetail;
        }
    }

    public boolean delete(Long id) {
        try (Connection conn = sql2o.beginTransaction()) {
            int result = conn.createQuery("delete from TransferDetail td where td.id=:id")
                    .addParameter("id", id)
                    .executeUpdate().getResult();
            return result > 0;
        }
    }

    public Long create(TransferDetail transferDetail) {
        String sql = "insert into TransferDetail(senderAccountId, receiverAccountId, isSuccessful, amount, transactionId) " +
                "values (:senderAccountId, :receiverAccountId, :isSuccessful, :amount, :transactionId)";
        try (Connection conn = sql2o.open()) {
            Object key = conn.createQuery(sql).bind(transferDetail).executeUpdate().getKey();
            return Long.parseLong(String.valueOf(key));
        }
    }
}