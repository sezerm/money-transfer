package com.sezer.moneytransfer;

import com.sezer.moneytransfer.api.*;
import com.sezer.moneytransfer.dao.*;
import com.sezer.moneytransfer.utils.InstanceFactory;
import com.p6spy.engine.spy.P6DataSource;
import org.flywaydb.core.Flyway;
import org.h2.jdbcx.JdbcConnectionPool;
import org.h2.tools.Server;
import org.sql2o.Sql2o;
import spark.ModelAndView;
import spark.TemplateEngine;
import spark.template.thymeleaf.ThymeleafTemplateEngine;

import javax.sql.DataSource;

import java.util.HashMap;
import java.util.Map;

import static spark.Spark.*;
import static spark.debug.DebugScreen.enableDebugScreen;

public class App {
    public static void main(String[] args) throws Exception {
        String url = "jdbc:h2:mem:test";
        String userName = "sa";
        String password = "";
        DataSource dataSource = JdbcConnectionPool.create(url, userName, password);
        dataSource = new P6DataSource(dataSource);

        Flyway flyway = new Flyway();
        flyway.setDataSource(dataSource);
        flyway.migrate();


        Sql2o sql2o = new Sql2o(dataSource);

        AccountDao accountDao = new AccountDao(sql2o);
        ClientDao clientDao = new ClientDao(sql2o);
        CurrencyDao currencyDao = new CurrencyDao(sql2o);
        CurrencyTypeDao currencyTypeDao = new CurrencyTypeDao(sql2o);
        TransactionDao transactionDao = new TransactionDao(sql2o);
        TransactionTypeDao transactionTypeDao = new TransactionTypeDao(sql2o);
        TransferDetailDao transferDetailDao = new TransferDetailDao(sql2o);

        //Add singleton instances
        InstanceFactory.addInstance(accountDao);
        InstanceFactory.addInstance(clientDao);
        InstanceFactory.addInstance(currencyDao);
        InstanceFactory.addInstance(currencyTypeDao);
        InstanceFactory.addInstance(transactionDao);
        InstanceFactory.addInstance(transactionTypeDao);
        InstanceFactory.addInstance(transferDetailDao);

        TemplateEngine templateEngine = new ThymeleafTemplateEngine();

        int maxThreads = 8;
        int minThreads = 2;
        int timeOutMillis = 30000;
        threadPool(maxThreads, minThreads, timeOutMillis);

        port(8080);
        staticFileLocation("/public");

        exception(Exception.class, (e, req, res) -> e.printStackTrace());
        before((req, res) ->
                System.out.println(req.pathInfo()));

        // Define rest endpoints
        path("/api", () -> {
            get("/accounts", AccountApi.listAccounts);
            get("/clients", ClientApi.listClient);
            get("/currencies", CurrencyApi.listCurrencies);
            get("/currencies-by-type", CurrencyApi.listCurrenciesByType);
            get("/currency-types", CurrencyTypeApi.listCurrencyTypes);
            get("/transactions", TransactionApi.listTransactions);
            get("/transfer-details", TransferDetailApi.listTransferDetails);
            post("/transfer-details", TransferDetailApi.transferMoney);
        });

        get("/index", (req, res) -> {
            Map<String, Object> map = new HashMap<>();
            map.put("version", "1.0.0");
            return new ModelAndView(map, "index");
        }, templateEngine);

        notFound((req, res) -> req.pathInfo() + "not found！");
        enableDebugScreen();
        awaitInitialization();
        Server.startWebServer(dataSource.getConnection());
    }
}
