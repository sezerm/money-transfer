package com.sezer.moneytransfer.dao;

import com.sezer.moneytransfer.entity.Client;
import com.sezer.moneytransfer.utils.Sql2oUtils;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import java.util.List;

public class ClientDao {

    private Sql2o sql2o;

    public ClientDao(Sql2o sql2o) {
        this.sql2o = sql2o;
    }

    public List<Client> list() {
        try (Connection conn = sql2o.beginTransaction()) {
            List<Client> clients = conn.createQuery("select * from Client")
                    .executeAndFetch(Client.class);
            return clients;
        }
    }

    public Client get(Long id) {
        try (Connection conn = sql2o.beginTransaction()) {
            Client client = conn.createQuery("select * from Client c where c.id=:id")
                    .addParameter("id", id)
                    .executeAndFetchFirst(Client.class);
            return client;
        }
    }

    public boolean delete(Long id) {
        try (Connection conn = sql2o.beginTransaction()) {
            int result = conn.createQuery("delete from Client c where c.id=:id")
                    .addParameter("id", id)
                    .executeUpdate().getResult();
            return result>0;
        }
    }

    public Long create(Client client) {
        String sql = "insert into User(name, surname, phone, address, email) values (:name, :surname, :phone, :address, :email)";
        try (Connection conn = sql2o.open()) {
            Object key = conn.createQuery(sql).bind(client).executeUpdate().getKey();
            return Long.parseLong(String.valueOf(key));
        }
    }
}
