package com.sezer.moneytransfer.service;

import com.sezer.moneytransfer.dao.CurrencyTypeDao;
import com.sezer.moneytransfer.entity.CurrencyType;
import com.sezer.moneytransfer.utils.InstanceFactory;

import java.util.List;

public class CurrencyTypeService {

    private static CurrencyTypeDao currencyTypeDao = InstanceFactory.getInstance(CurrencyTypeDao.class);

    public List<CurrencyType> list() {
        return currencyTypeDao.list();
    }
}
