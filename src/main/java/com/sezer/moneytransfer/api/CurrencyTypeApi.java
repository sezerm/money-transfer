package com.sezer.moneytransfer.api;

import com.google.gson.Gson;
import com.sezer.moneytransfer.service.CurrencyTypeService;
import com.sezer.moneytransfer.utils.InstanceFactory;
import spark.Route;

public class CurrencyTypeApi {

    private static CurrencyTypeService currencyTypeService = InstanceFactory.getInstance(CurrencyTypeService.class);

    public static Route listCurrencyTypes = (req, res) -> {
        res.type("application/json");
        return new Gson().toJson(currencyTypeService.list());
    };

}
