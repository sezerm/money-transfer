package com.sezer.moneytransfer.service;

import com.sezer.moneytransfer.dao.TransferDetailDao;
import com.sezer.moneytransfer.entity.Account;
import com.sezer.moneytransfer.entity.Transaction;
import com.sezer.moneytransfer.entity.TransferDetail;
import com.sezer.moneytransfer.enums.TransactionTypeEnum;
import com.sezer.moneytransfer.exception.MoneyTransferException;
import com.sezer.moneytransfer.utils.InstanceFactory;
import com.sezer.moneytransfer.vm.request.MoneyTransferVM;

import java.math.BigDecimal;
import java.util.List;

public class TransferDetailService {

    private static TransferDetailDao transferDetailDao = InstanceFactory.getInstance(TransferDetailDao.class);
    private static AccountService accountService = InstanceFactory.getInstance(AccountService.class);
    private static TransactionService transactionService = InstanceFactory.getInstance(TransactionService.class);

    private static final String INVALID_ACCOUNT_NUMBER = "Sender or receiver account is invalid.";
    private static final String SUCCESSFUL_MESSAGE = "Successful transfer.";
    private static final String INSUFFICIENT_BALANCE = "Transfer failed because of insufficient balance.";
    private static final String WRONG_CURRENCY = "Transfer failed because of currency inconsistency";

    public List<TransferDetail> list() {
        return transferDetailDao.list();
    }

    public void transferMoney(MoneyTransferVM moneyTransferVM) throws MoneyTransferException {
        Account senderAccount = accountService.getByAccountNumber(moneyTransferVM.getSenderAccountNumber());
        Account receiverAccount = accountService.getByAccountNumber(moneyTransferVM.getReceiverAccountNumber());

        String message = SUCCESSFUL_MESSAGE;
        boolean isTransferSuccessful = false;

        // Check account validity
        if (senderAccount != null && receiverAccount != null) {
            Transaction transaction = new Transaction();
            transaction.setTransactionTypeId(TransactionTypeEnum.MONEY_TRANSFER.getId());

            Long currencyId = moneyTransferVM.getCurrencyId();

            TransferDetail transferDetail = new TransferDetail();
            transferDetail.setSenderAccountId(senderAccount.getId());
            transferDetail.setReceiverAccountId(receiverAccount.getId());
            BigDecimal amount = moneyTransferVM.getAmount();
            transferDetail.setAmount(amount);

            // If sender account and receiver account currencies are same and
            // If there is enough balance transfer money and arrange balances
            if (currencyId != null && currencyId.equals(receiverAccount.getCurrencyId()) &&
                    currencyId.equals(receiverAccount.getCurrencyId())) {
                BigDecimal senderAccountAsset = senderAccount.getAsset();
                if (senderAccountAsset.compareTo(amount) > -1) {
                    senderAccount.setAsset(senderAccountAsset.subtract(amount));
                    receiverAccount.setAsset(receiverAccount.getAsset().add(amount));
                    accountService.update(senderAccount);
                    accountService.update(receiverAccount);
                    transferDetail.setIsSuccessful(true);
                    isTransferSuccessful = true;
                } else {
                    message = INSUFFICIENT_BALANCE;
                }
            } else
                message = WRONG_CURRENCY;

            transaction.setDetail(message);
            Long transactionId = transactionService.create(transaction);
            transferDetail.setSuccessful(isTransferSuccessful);
            transferDetail.setTransactionId(transactionId);
            transferDetailDao.create(transferDetail);
        } else
            message = INVALID_ACCOUNT_NUMBER;
        if (!isTransferSuccessful) {
            throw new MoneyTransferException(message);
        }
    }
}
