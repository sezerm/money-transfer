package com.sezer.moneytransfer.dao;

import com.sezer.moneytransfer.entity.Currency;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import java.util.List;

public class CurrencyDao {

    private Sql2o sql2o;

    public CurrencyDao(Sql2o sql2o) {
        this.sql2o = sql2o;
    }

    public List<Currency> list() {
        try (Connection conn = sql2o.beginTransaction()) {
            List<Currency> currencies = conn.createQuery("select * from Currency")
                    .executeAndFetch(Currency.class);
            return currencies;
        }
    }

    public List<Currency> listByTypeId(Long typeId) {
        try (Connection conn = sql2o.beginTransaction()) {
            List<Currency> currencies = conn.createQuery("select * from Currency where currencyTypeId = :typeId")
                    .executeAndFetch(Currency.class);
            return currencies;
        }
    }
}
