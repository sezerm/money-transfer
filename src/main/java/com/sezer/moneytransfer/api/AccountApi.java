package com.sezer.moneytransfer.api;

import com.google.gson.Gson;
import com.sezer.moneytransfer.service.AccountService;
import com.sezer.moneytransfer.service.ClientService;
import com.sezer.moneytransfer.utils.InstanceFactory;
import spark.Route;

public class AccountApi {

    private static AccountService accountService = InstanceFactory.getInstance(AccountService.class);

    public static Route listAccounts = (req, res) -> {
        res.type("application/json");
        return new Gson().toJson(accountService.list());
    };

}
