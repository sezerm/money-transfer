package com.sezer.moneytransfer.dao;

import com.sezer.moneytransfer.entity.CurrencyType;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import java.util.List;

public class CurrencyTypeDao {

    private Sql2o sql2o;

    public CurrencyTypeDao(Sql2o sql2o) {
        this.sql2o = sql2o;
    }

    public List<CurrencyType> list() {
        try (Connection conn = sql2o.beginTransaction()) {
            List<CurrencyType> currencies = conn.createQuery("select * from CurrencyType")
                    .executeAndFetch(CurrencyType.class);
            return currencies;
        }
    }
}
