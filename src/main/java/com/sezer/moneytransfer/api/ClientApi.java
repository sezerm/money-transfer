package com.sezer.moneytransfer.api;

import com.google.gson.Gson;
import com.sezer.moneytransfer.service.ClientService;
import com.sezer.moneytransfer.utils.InstanceFactory;
import spark.Route;

public class ClientApi {

    private static ClientService clientService = InstanceFactory.getInstance(ClientService.class);

    public static Route listClient = (req, res) -> {
        res.type("application/json");
        return new Gson().toJson(clientService.list());
    };

}
